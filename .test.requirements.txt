# This is a pip requirements.txt file for running bridgedb's tests.
#
# To run install these dependencies and run the tests, do:
#
#     $ pip install -r .test.requirements.txt
#     $ make coverage
#
coverage==6.4.4
mechanize==0.4.8
pycodestyle==2.9.1
pylint==2.15.2
sure==2.0.0
