# Translations template for bridgedb.
# Copyright (C) 2020 'The Tor Project, Inc.'
# This file is distributed under the same license as the bridgedb project.
# 
# Translators:
# ilbe123 <a3057016@drdrb.net>, 2014
# Chris Park <utopinator@gmail.com>, 2016
# snotree <cknblue@gmail.com>, 2017
# cwt96 <cwt967@naver.com>, 2012
# 고영욱 <greenknite@naver.com>, 2015
# Hyuntae Chun, 2017
# ebde8080c092fc908416083a701feb30, 2018
# Jamin <bakzaa@gmail.com>, 2014
# Dr.what <javrick6@naver.com>, 2014-2015
# Gu Hong Min <placidmoon1@gmail.com>, 2015
# 류종헌, 2015
# Myeongjin <aranet100@gmail.com>, 2016
# ebde8080c092fc908416083a701feb30, 2019
# 이피시소리 <pcsori@gmail.com>, 2012
# Philipp Sauter <qt123@pm.me>, 2018
# Sam Ryoo <samryoo@gmail.com>, 2014
# Sangmin Lee <sangminlee7648@gmail.com>, 2016
# woo lee <leewoo0614@gmail.com>, 2019
# Revi_, 2014
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: 'https://trac.torproject.org/projects/tor/newticket?component=BridgeDB&keywords=bridgedb-reported,msgid&cc=isis,sysrqb&owner=isis'\n"
"POT-Creation-Date: 2020-05-14 14:21-0700\n"
"PO-Revision-Date: 2020-05-15 08:24+0000\n"
"Last-Translator: Transifex Bot <>\n"
"Language-Team: Korean (http://www.transifex.com/otf/torproject/language/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. -*- coding: utf-8 ; test-case-name: bridgedb.test.test_https_server -*-
#. This file is part of BridgeDB, a Tor bridge distribution system.
#. :authors: please see included AUTHORS file
#. :copyright: (c) 2007-2017, The Tor Project, Inc.
#. (c) 2013-2017, Isis Lovecruft
#. :license: see LICENSE for licensing information
#. : The path to the HTTPS distributor's web templates.  (Should be the
#. : "templates" directory in the same directory as this file.)
#. Setting `filesystem_checks` to False is recommended for production servers,
#. due to potential speed increases. This means that the atimes of the Mako
#. template files aren't rechecked every time the template is requested
#. (otherwise, if they are checked, and the atime is newer, the template is
#. recompiled). `collection_size` sets the number of compiled templates which
#. are cached before the least recently used ones are removed. See:
#. http://docs.makotemplates.org/en/latest/usage.html#using-templatelookup
#. : A list of supported language tuples. Use getSortedLangList() to read this
#. variable.
#. We use our metrics singleton to keep track of BridgeDB metrics such as
#. "number of failed HTTPS bridge requests."
#. Convert all key/value pairs from bytes to str.
#. TRANSLATORS: Please DO NOT translate the following words and/or phrases in
#. any string (regardless of capitalization and/or punctuation):
#. "BridgeDB"
#. "pluggable transport"
#. "pluggable transports"
#. "obfs4"
#. "Tor"
#. "Tor Browser"
#: bridgedb/distributors/https/server.py:154
msgid "Sorry! Something went wrong with your request."
msgstr "죄송합니다! 당신의 요청에 무언가 문제가 있습니다."

#: bridgedb/distributors/https/templates/base.html:42
msgid "Language"
msgstr "언어"

#: bridgedb/distributors/https/templates/base.html:94
msgid "Report a Bug"
msgstr "버그를 보고하기"

#: bridgedb/distributors/https/templates/base.html:97
msgid "Source Code"
msgstr "소스 코드"

#: bridgedb/distributors/https/templates/base.html:100
msgid "Changelog"
msgstr "변경 기록"

#: bridgedb/distributors/https/templates/bridges.html:35
msgid "Select All"
msgstr "모두 선택"

#: bridgedb/distributors/https/templates/bridges.html:40
msgid "Show QRCode"
msgstr "QR코드 보기"

#: bridgedb/distributors/https/templates/bridges.html:52
msgid "QRCode for your bridge lines"
msgstr "브릿지 라인을 위한 QR코드"

#: bridgedb/distributors/https/templates/bridges.html:63
msgid "It seems there was an error getting your QRCode."
msgstr "QR 코드를 불러 오는 중 오류가 생긴 것 같습니다."

#: bridgedb/distributors/https/templates/bridges.html:68
msgid ""
"This QRCode contains your bridge lines. Scan it with a QRCode reader to copy"
" your bridge lines onto mobile and other devices."
msgstr "이 QR코드는 당신의 브릿지 행을 포함하고 있습니다. 브릿지 행을 다른 기기로 복사하려면 QR코드 리더로 스캔하세요."

#: bridgedb/distributors/https/templates/bridges.html:110
msgid "BridgeDB encountered an error."
msgstr ""

#: bridgedb/distributors/https/templates/bridges.html:116
msgid "There currently aren't any bridges available..."
msgstr "현재 사용할 수 있는 브릿지가 없습니다..."

#: bridgedb/distributors/https/templates/bridges.html:118
#: bridgedb/distributors/https/templates/bridges.html:122
#, python-format
msgid ""
" Perhaps you should try %s going back %s and choosing a different bridge "
"type!"
msgstr "%s 되돌아가서 %s 다른 브릿지 타입도 골라보세요!"

#: bridgedb/distributors/https/templates/index.html:11
#, python-format
msgid "Step %s1%s"
msgstr "%s1%s단계 "

#: bridgedb/distributors/https/templates/index.html:13
#, python-format
msgid "Download %s Tor Browser %s"
msgstr "%s Tor 브라우저 %s를 다운받습니다 "

#: bridgedb/distributors/https/templates/index.html:25
#, python-format
msgid "Step %s2%s"
msgstr "%s2%s단계 "

#: bridgedb/distributors/https/templates/index.html:28
#: bridgedb/distributors/https/templates/index.html:30
#, python-format
msgid "Get %s bridges %s"
msgstr "%s 브릿지 %s를 가져옵니다"

#: bridgedb/distributors/https/templates/index.html:40
#, python-format
msgid "Step %s3%s"
msgstr "%s3%s단계 "

#: bridgedb/distributors/https/templates/index.html:43
#: bridgedb/distributors/https/templates/index.html:47
#, python-format
msgid "Now %s add the bridges to Tor Browser %s"
msgstr "이제 %s 브릿지를 Tor 브라우저에 추가합니다 %s"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. (These are used to insert HTML5 underlining tags, to mark accesskeys
#. for disabled users.)
#: bridgedb/distributors/https/templates/options.html:42
#, python-format
msgid "%sJ%sust give me bridges!"
msgstr "%s그%s냥 브릿지 주세요!"

#: bridgedb/distributors/https/templates/options.html:55
msgid "Advanced Options"
msgstr "고급 옵션"

#: bridgedb/distributors/https/templates/options.html:93
msgid "No"
msgstr "아니오"

#: bridgedb/distributors/https/templates/options.html:94
msgid "none"
msgstr "없음"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Translate "Yes!" as in "Yes! I do need IPv6 addresses."
#: bridgedb/distributors/https/templates/options.html:131
#, python-format
msgid "%sY%ses!"
msgstr "%s네!%s"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Please do NOT translate the word "bridge"!
#: bridgedb/distributors/https/templates/options.html:154
#, python-format
msgid "%sG%set Bridges"
msgstr "%s브%s릿지 얻기"

#: bridgedb/strings.py:33
msgid "[This is an automated email.]"
msgstr ""

#: bridgedb/strings.py:35
msgid "Here are your bridges:"
msgstr "당신의 브릿지는 다음과 같습니다:"

#: bridgedb/strings.py:37
#, python-format
msgid ""
"You have exceeded the rate limit. Please slow down! The minimum time between\n"
"emails is %s hours. All further emails during this time period will be ignored."
msgstr "속도 제한을 초과하였습니다. 속도를 낮추세요! 이메일 간의 최소 간격은\n%s 시간입니다. 이 기간 동안의 추가적인 모든 이메일은 무시될 것입니다."

#: bridgedb/strings.py:40
msgid ""
"If these bridges are not what you need, reply to this email with one of\n"
"the following commands in the message body:"
msgstr ""

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#. TRANSLATORS: Please DO NOT translate "Tor".
#. TRANSLATORS: Please DO NOT translate "Tor Network".
#: bridgedb/strings.py:50
#, python-format
msgid ""
"BridgeDB can provide bridges with several %stypes of Pluggable Transports%s,\n"
"which can help obfuscate your connections to the Tor Network, making it more\n"
"difficult for anyone watching your internet traffic to determine that you are\n"
"using Tor.\n"
"\n"
msgstr "BridgeDB 는 여러 종류의 %s삽입형 장치%s 와의 브릿지를 제공합니다.\n이것은 Tor 네트워크와의 연결을 난독화 시키며 누군가 인터넷 트래픽을\n감시하는 사람이 당신이 Tor 네트워크를 사용하고 있다는 것을 식별하기 어렵게 합니다.\n\n"

#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#: bridgedb/strings.py:57
msgid ""
"Some bridges with IPv6 addresses are also available, though some Pluggable\n"
"Transports aren't IPv6 compatible.\n"
"\n"
msgstr "몇몇 브리지는 IPv6 주소도 지원하나,\nPluggable Transports는 IPv6를 지원하지 않습니다.\n\n"

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: The phrase "plain-ol'-vanilla" means "plain, boring,
#. regular, or unexciting". Like vanilla ice cream. It refers to bridges
#. which do not have Pluggable Transports, and only speak the regular,
#. boring Tor protocol. Translate it as you see fit. Have fun with it.
#: bridgedb/strings.py:66
#, python-format
msgid ""
"Additionally, BridgeDB has plenty of plain-ol'-vanilla bridges %s without any\n"
"Pluggable Transports %s which maybe doesn't sound as cool, but they can still\n"
"help to circumvent internet censorship in many cases.\n"
"\n"
msgstr "거기다, BridgeDB는 %s Pluggable Transport가 없는 %s\n흔해빠진 브릿지들도 많아요. 대단한 소리같지 않겠지만,\n그래도 여러 모로 인터넷 검열 우회를 도와줍니다.\n\n"

#: bridgedb/strings.py:78 bridgedb/test/test_https.py:356
msgid "What are bridges?"
msgstr "'브릿지'란?"

#: bridgedb/strings.py:79
#, python-format
msgid "%s Bridges %s are Tor relays that help you circumvent censorship."
msgstr "%s Bridges %s는 당신이 검열을 피할수 있게 하는 Tor 릴레이입니다. "

#: bridgedb/strings.py:84
msgid "I need an alternative way of getting bridges!"
msgstr "브릿지를 얻는 다른 방법이 없을까요?"

#. TRANSLATORS: Please DO NOT translate "get transport obfs4".
#: bridgedb/strings.py:86
#, python-format
msgid ""
"Another way to get bridges is to send an email to %s. Leave the email subject\n"
"empty and write \"get transport obfs4\" in the email's message body. Please note\n"
"that you must send the email using an address from one of the following email\n"
"providers: %s or %s."
msgstr ""

#: bridgedb/strings.py:94
msgid "My bridges don't work! I need help!"
msgstr "브릿지가 맛이 갔어요! 도와줘요!"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#. TRANSLATORS: The two '%s' are substituted with "Tor Browser Manual" and
#. "Support Portal", respectively.
#: bridgedb/strings.py:98
#, python-format
msgid ""
"If your Tor Browser cannot connect, please take a look at the %s and our %s."
msgstr ""

#: bridgedb/strings.py:102
msgid "Here are your bridge lines:"
msgstr "당신의 브릿지 행들은 다음과 같습니다:"

#: bridgedb/strings.py:103
msgid "Get Bridges!"
msgstr "브릿지를 얻으세요!"

#: bridgedb/strings.py:107
msgid "Bridge distribution mechanisms"
msgstr ""

#. TRANSLATORS: Please DO NOT translate "BridgeDB", "HTTPS", and "Moat".
#: bridgedb/strings.py:109
#, python-format
msgid ""
"BridgeDB implements four mechanisms to distribute bridges: \"HTTPS\", \"Moat\",\n"
"\"Email\", and \"Reserved\".  Bridges that are not distributed over BridgeDB use\n"
"the pseudo-mechanism \"None\".  The following list briefly explains how these\n"
"mechanisms work and our %sBridgeDB metrics%s visualize how popular each of the\n"
"mechanisms is."
msgstr ""

#: bridgedb/strings.py:115
#, python-format
msgid ""
"The \"HTTPS\" distribution mechanism hands out bridges over this website.  To get\n"
"bridges, go to %sbridges.torproject.org%s, select your preferred options, and\n"
"solve the subsequent CAPTCHA."
msgstr ""

#: bridgedb/strings.py:119
#, python-format
msgid ""
"The \"Moat\" distribution mechanism is part of Tor Browser, allowing users to\n"
"request bridges from inside their Tor Browser settings.  To get bridges, go to\n"
"your Tor Browser's %sTor settings%s, click on \"request a new bridge\", solve the\n"
"subsequent CAPTCHA, and Tor Browser will automatically add your new\n"
"bridges."
msgstr ""

#: bridgedb/strings.py:125
#, python-format
msgid ""
"Users can request bridges from the \"Email\" distribution mechanism by sending an\n"
"email to %sbridges@torproject.org%s and writing \"get transport obfs4\" in the\n"
"email body."
msgstr ""

#: bridgedb/strings.py:129
msgid "Reserved"
msgstr ""

#: bridgedb/strings.py:130
#, python-format
msgid ""
"BridgeDB maintains a small number of bridges that are not distributed\n"
"automatically.  Instead, we reserve these bridges for manual distribution and\n"
"hand them out to NGOs and other organizations and individuals that need\n"
"bridges.  Bridges that are distributed over the \"Reserved\" mechanism may not\n"
"see users for a long time.  Note that the \"Reserved\" distribution mechanism is\n"
"called \"Unallocated\" in %sbridge pool assignment%s files."
msgstr ""

#: bridgedb/strings.py:137
msgid "None"
msgstr "없음"

#: bridgedb/strings.py:138
msgid ""
"Bridges whose distribution mechanism is \"None\" are not distributed by BridgeDB.\n"
"It is the bridge operator's responsibility to distribute their bridges to\n"
"users.  Note that on Relay Search, a freshly set up bridge's distribution\n"
"mechanism says \"None\" for up to approximately one day.  Be a bit patient, and\n"
"it will then change to the bridge's actual distribution mechanism.\n"
msgstr ""

#: bridgedb/strings.py:148
msgid "Please select options for bridge type:"
msgstr "브릿지 종류에 관한 옵션을 골라 주세요:"

#: bridgedb/strings.py:149
msgid "Do you need IPv6 addresses?"
msgstr "IPv6 주소가 필요합니까?"

#: bridgedb/strings.py:150
#, python-format
msgid "Do you need a %s?"
msgstr "%s 가 필요합니까?"

#: bridgedb/strings.py:154
msgid "Your browser is not displaying images properly."
msgstr "브라우저가 사진을 정상적으로 불러오지 못합니다."

#: bridgedb/strings.py:155
msgid "Enter the characters from the image above..."
msgstr "위 그림의 문자를 입력하십시오..."

#: bridgedb/strings.py:159
msgid "How to start using your bridges"
msgstr "브릿지를 사용하는 방법"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#: bridgedb/strings.py:161
#, python-format
msgid ""
" First, you need to %sdownload Tor Browser%s. Our Tor Browser User\n"
" Manual explains how you can add your bridges to Tor Browser. If you are\n"
" using Windows, Linux, or OS X, %sclick here%s to learn more. If you\n"
" are using Android, %sclick here%s."
msgstr ""

#: bridgedb/strings.py:166
msgid ""
"Add these bridges to your Tor Browser by opening your browser\n"
"preferences, clicking on \"Tor\", and then adding them to the \"Provide a\n"
"bridge\" field."
msgstr ""

#: bridgedb/strings.py:173
msgid "(Request unobfuscated Tor bridges.)"
msgstr ""

#: bridgedb/strings.py:174
msgid "(Request IPv6 bridges.)"
msgstr ""

#: bridgedb/strings.py:175
msgid "(Request obfs4 obfuscated bridges.)"
msgstr ""
